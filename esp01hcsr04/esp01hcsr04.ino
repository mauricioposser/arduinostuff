/* 
 *  Leitura da distância via sensor HC-SR04 enviando os dados para um Mqtt Broker
 *  Código para o ESP-01
 *  Utiliza o WifiManager para configurar a rede e também o mqtt broker.
 *  Portal de configuração no IP: 192.168.4.1 (padrão).
 *  
 *  Código e montagem inspirado nos seguintes (excelentes) canais do youtube:
 *  Fernando K Tecnologia: https://www.youtube.com/channel/UCIRfWLRDsdVOjJfpy6Ne04Q
 *  Infortrônica Para Zumbis: https://www.youtube.com/channel/UC05P95nXawYc15gIpN8GFPw
 *  
 *  Registro aqui meu muito obrigado a estes dois abnegados que compartilham seus conhecimentos com desconhecidos como eu! :)
 *  
 */

// Bibliotecas
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <DNSServer.h>
#include <WiFiManager.h>
#include <FS.h>
#include <ArduinoJson.h>
#include <PubSubClient.h>

#define LOOPDELAY 5000  // 5 segundos para cada leitura do sensor e publicação no Mqtt Broker

// Configuração padrão do Mqtt Broker
#define MQTTSERVER         "BROKER"  //URL do servidor MQTT
#define MQTTSERVERPORT     "PORT"              //Porta do servidor MQTT
#define MQTTSERVERUSER     "USER"           //Usuário
#define MQTTSERVERPASSWORD "PASSWORD"       //Senha
#define MQTTPUBTOPICWD     "esp8266/whatchdog"  // Contador de [0; 100] para monitoramento de comuinicação
#define MQTTPUBTOPICHC     "esp8266/hcsr04"  // Distância (cm) medida pelo sensor HC-SR04

// Configuração dos pinos do ESP8266 NodeMcu
const int PIN_ECHO = 3;  //pino Echo    - HC-SR04
const int PIN_TRIG = 2; // pino Trigger - HC-SR04
const int PIN_AP = 1;  //pino do botão para resetar a rede

int counter = 0;      // Contador de 0 a 100 para monitoramento de funcionamento
bool saveWifiCfg = false;  // Controle para salvamento dos dados

WiFiClient espClient;            //Instância do WiFiClient
PubSubClient client(espClient);  //Passando a instância do WiFiClient para a instância do PubSubClient


/////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funções auxiliares
/////////////////////////////////////////////////////////////////////////////////////////////////////////

// Função para leitura do sensor e composição da mensagem para publuicação no Mqtt Broker
String readSensor2Mqtt()
{

  // Garante que trigger está desligado
  digitalWrite(PIN_TRIG, LOW);
  delayMicroseconds(2);
  // Dispara o trigger
  digitalWrite(PIN_TRIG, HIGH);
  delayMicroseconds(10);
  digitalWrite(PIN_TRIG, LOW);
  // Recebe o resultado - verifica o período que ficou em HIGH
  unsigned long period = pulseIn(PIN_ECHO, HIGH);
  // Calcula a distância em cm
  float distance = period / 58;
  return String(distance);
}
 

// *** WifiManager ***
//Função de retorno para notificar sobre a necessidade de salvar as configurações
void saveCallback()
{
  saveWifiCfg = true;
}

// Callback que informa que o ESP entrou no modo AP
void configModeCallback (WiFiManager *myWiFiManager) {  
  counter = 999;
  //Serial.println("Modo de configuração ...");
  //Serial.println(WiFi.softAPIP()); //imprime o IP do AP
  //Serial.println(myWiFiManager->getConfigPortalSSID()); //imprime o SSID criado da rede
}

// Callback que informa que as configs. da nova rede foram salvas (modo estação)
void saveConfigCallback () {
  counter = 111;
  //Serial.println("Configuração salva!");
  //Serial.println(WiFi.softAPIP()); //imprime o IP do AP
}

// *** MQTT (pub/sub) ***
//Função que reconecta ao servidor MQTT
void reconnectMqtt()
{
  //Repete até conectar
  while(!client.connected()) {
    //Tentativa de conectar. Se o MQTT precisa de autenticação, será chamada a função com autenticação, caso contrário, chama a sem autenticação. 
    bool conectado = strlen(MQTTSERVERUSER) > 0 ?
                     client.connect("ESP8266Client", MQTTSERVERUSER, MQTTSERVERPASSWORD):
                     client.connect("ESP8266Client");
    if(!conectado) {
      //Aguarda 5 segundos para tentar novamente
      delay(5000);
    }
  }
}

void disconnectMqtt()
{
  client.disconnect();
}

//Função que envia os dados de intensidade luminosa.
void pubMqttBroker()
{
  if (!client.connected()) {
    reconnectMqtt();
  }
  client.loop();
  //Publicando no MQTT
  String strMqttMsg = readSensor2Mqtt();
  char* msgMqtt = new char[strMqttMsg.length() + 1];
  strcpy(msgMqtt, strMqttMsg.c_str());
  // Publicação no tópico  MQTTPUBTOPICHC
  client.publish(MQTTPUBTOPICHC, msgMqtt, true);
  delete [] msgMqtt;
  if(counter > 100)
    counter = -1;
  counter += 1;
  String countStr = String(counter);
  char* msg2Mqtt = new char[countStr.length() + 1];
  strcpy(msg2Mqtt, countStr.c_str());
  // Publicação no tópico  MQTTPUBTOPICWD
  client.publish(MQTTPUBTOPICWD, msg2Mqtt, true);
  delete [] msg2Mqtt;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funções SETUP & LOOP
/////////////////////////////////////////////////////////////////////////////////////////////////////////

void setup()
{
  // Serial.begin(9600);
  pinMode(0, OUTPUT);
  pinMode(PIN_TRIG, OUTPUT);
  pinMode(PIN_ECHO, INPUT);
  pinMode(PIN_AP, INPUT);

  digitalWrite(0, HIGH);
  digitalWrite(PIN_TRIG, HIGH);
  digitalWrite(0, LOW);
  digitalWrite(PIN_TRIG, LOW);
  digitalWrite(PIN_AP, LOW); //Define o botão como desligado
  

  //Parâmetros extras para configuração via WifiManager
  //Depois de conectar, parameter.getValue() vai pegar o valor configurado.
  //Os campos do WiFiManagerParameter são: id do parâmetro, nome, valor padrão, comprimento
  WiFiManagerParameter custom_mqtt_server("server", "Servidor MQTT", MQTTSERVER, 40);
  WiFiManagerParameter custom_mqtt_port("port", "Porta", MQTTSERVERPORT, 6);
  WiFiManagerParameter custom_mqtt_user("user", "Usuario", MQTTSERVERUSER, 20);
  WiFiManagerParameter custom_mqtt_pass("pass", "Senha", MQTTSERVERPASSWORD, 20);
  WiFiManagerParameter custom_mqtt_topic_pub("topic_pub", "Topico para publicar", MQTTPUBTOPICHC, 30);
  WiFiManagerParameter custom_mqtt_topic_pubwd("topic_pubwd", "Topico para publicar", MQTTPUBTOPICWD, 30);
  
  //declaração do objeto wifiManager
  WiFiManager wifiManager;

  //callback para quando entra em modo de configuração AP
  wifiManager.setAPCallback(configModeCallback); 
  //Definindo a função que informará a necessidade de salvar as configurações
  wifiManager.setSaveConfigCallback(saveCallback);
  
  //Adicionando os parâmetros para conectar ao servidor MQTT
  wifiManager.addParameter(&custom_mqtt_server);
  wifiManager.addParameter(&custom_mqtt_port);
  wifiManager.addParameter(&custom_mqtt_user);
  wifiManager.addParameter(&custom_mqtt_pass);
  wifiManager.addParameter(&custom_mqtt_topic_pub);
  wifiManager.addParameter(&custom_mqtt_topic_pubwd);
  wifiManager.autoConnect("ESP8266_AP","esp12345"); //cria uma rede sem senha

  //Formatando a memória interna
  //(descomente a linha abaixo enquanto estiver testando e comente ou apague quando estiver pronto)
  //SPIFFS.format();

  //////////////////////////////////////////////////////////////////////////////////////////////////////
  //Iniciando o SPIFSS (SPI Flash File System)
  if (SPIFFS.begin()) {
    if (SPIFFS.exists("/config.json")) {
      //Arquivo de configuração existe e será lido.
      File configFile = SPIFFS.open("/config.json", "r");
      if (configFile) {
        size_t size = configFile.size();
        //Alocando um buffer para armazenar o conteúdo do arquivo.
        std::unique_ptr<char[]> buf(new char[size]);
        configFile.readBytes(buf.get(), size);
        // Versão 5
        //DynamicJsonBuffer jsonBuffer; // Versão 5
        //JsonObject& json = jsonBuffer.parseObject(buf.get());
        //json.printTo(Serial);
        //if (json.success())
        DynamicJsonDocument  json(1024); // versão 6
        auto error = deserializeJson(json, buf.get());
        serializeJson(json, Serial); // 6
        if (!error)
        {
            //Copiando as variáveis salvas previamente no aquivo json para a memória do ESP.
            strcpy(MQTTSERVER, json["MQTTSERVER"]);
            strcpy(MQTTSERVERPORT, json["MQTTSERVERPORT"]);
            strcpy(MQTTSERVERUSER, json["MQTTSERVERUSER"]);
            strcpy(MQTTSERVERPASSWORD, json["MQTTSERVERPASSWORD"]);
            strcpy(MQTTPUBTOPICHC, json["MQTTPUBTOPICHC"]);
            strcpy(MQTTPUBTOPICWD, json["MQTTPUBTOPICWD"]);
        } else {
          //Falha ao ler as configuracoes do arquivo json
          counter = 222;
        }
      }
    }
  } else {
    // Falha ao montar o sistema de arquivos SPIFSS.
    counter = 333;
  }
  //Fim da leitura do sistema de arquivos SPIFSS
  //////////////////////////////////////////////////////////////////////////////////////////////////////

  //Busca o ID e senha da rede wifi e tenta conectar.
  //Caso não consiga conectar ou não exista ID e senha,
  //cria um access point com o nome "ESP8266_AP" e a senha "esp12345"
  //E entra em loop aguardando a configuração de uma rede WiFi válida.
  if (!wifiManager.autoConnect("ESP8266_AP", "esp12345")) {
    // Falha ao conectar. Excedeu o tempo limite para conexao.
    delay(3000);
    //Reinicia o ESP e tenta novamente
    ESP.reset();
    delay(5000);
  }
  //WiFi conectado
  //Lendo os parâmetros atualizados do Mqtt Broker
  strcpy(MQTTSERVER, custom_mqtt_server.getValue());
  strcpy(MQTTSERVERPORT, custom_mqtt_port.getValue());
  strcpy(MQTTSERVERUSER, custom_mqtt_user.getValue());
  strcpy(MQTTSERVERPASSWORD, custom_mqtt_pass.getValue());
  strcpy(MQTTPUBTOPICHC, custom_mqtt_topic_pub.getValue());
  strcpy(MQTTPUBTOPICWD, custom_mqtt_topic_pubwd.getValue());
  //Salvando os parâmetros informados na tela web do WiFiManager
  if (saveWifiCfg)
  {
    DynamicJsonDocument  json(1024); // versão 6
    json["MQTTSERVER"] = MQTTSERVER;
    json["MQTTSERVERPORT"] = MQTTSERVERPORT;
    json["MQTTSERVERUSER"] = MQTTSERVERUSER;
    json["MQTTSERVERPASSWORD"] = MQTTSERVERPASSWORD;
    json["MQTTPUBTOPICHC"] = MQTTPUBTOPICHC;
    json["MQTTPUBTOPICWD"] = MQTTPUBTOPICWD;

    File configFile = SPIFFS.open("/config.json", "w");
    if (!configFile)
    {
      // Houve uma falha ao abrir o arquivo de configuracao para incluir/alterar as configuracoes.
      counter = 444;
    }
    serializeJson(json, Serial); // 6
    serializeJson(json, configFile); // 6
    configFile.close();
  }

  //Informando ao client do PubSub a url do servidor e a porta.
  int portaInt = atoi(MQTTSERVERPORT);
  client.setServer(MQTTSERVER, portaInt);
}

void loop()
{
  WiFiManager wifiManager;
  
  //se o botão foi pressionado
  if ( digitalRead(PIN_AP) == HIGH ) {
      if(!wifiManager.startConfigPortal("ESP8266_AP", "esp12345") ){
        delay(1000);
        ESP.restart();
        delay(1000);
      }
  }
  else
  {
    pubMqttBroker();
    disconnectMqtt();
    delay(LOOPDELAY);
  }
}
