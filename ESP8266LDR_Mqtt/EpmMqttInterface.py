# -*- coding: utf-8 -*-
"""Elipse Plant Manager - Palestra sobre sistema de gerenciamento de dados de processo
Copyright (C) 2018 Elipse Software.
Distributed under the MIT License.
(See accompanying file LICENSE.txt or copy at http://opensource.org/licenses/MIT)

Requisitos Python
   - Python 3.6
   - módulo Mqtt paho
   - módulo epmwebapi

Outros programas:
   -  Broker MQTT (ex.: mosquitto)
   - EPM Server
   - Um EPM Webserver
"""

__author__ = u"Maurício Simões Posser"
__copyright__ = "Copyright 2018, Elipse Software"
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = u"Maurício Simões Posser"
__email__ = "mauricio@elipse.com.br"
__status__ = "Production"

import sys
import datetime
import paho.mqtt.client as mqttc
import epmwebapi as epm
from epmwebapi.dataobjectattributes import DataObjectAttributes
from epmwebapi.dataobjectsfilter import DataObjectsFilter
from epmwebapi.dataobjectsfilter import DataObjectsFilterType
from epmwebapi.domainfilter import DomainFilter


# WebServer: Nome da máquina onde está rodando o EPM Webserver
# UserName: nome do usuário do EPM Server
# Password: sena do usuário do EPM Server
# mspesp8266_pinldr: Basic Variable previamente criada no EPM Server
webServer = epm.EpmConnection('http://WebServer:44333', 'http://WebServer:44332', 'UserName', 'Password')
dictDO = webServer.getBasicVariables(['mspesp8266_pinldr'])
mspesp8266_pinldr = dictDO['mspesp8266_pinldr']
print('Dicionário com BV: {}'.format(dictDO))

# Subscribe tópico
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("mspesp8266/pinldr")

def on_message(client, userdata, msg):
    global mspesp8266_pinldr
    print(msg.topic+" "+str(msg.payload))
    timestamp = datetime.datetime.utcnow().replace(microsecond=0)
    value = float(msg.payload)
    mspesp8266_pinldr.write(value, timestamp, 0)

def on_disconnect(client, userdata, rc):
    global webServer
    webServer.close()
    if rc != 0:
        print("Unexpected disconnection!!!")

# Configurações do cliente Mqtt - e suas callbacks
client = mqttc.Client()
client.on_connect = on_connect
client.on_message = on_message
client.on_disconnect = on_disconnect
client.disable_logger()

# Define um usuário e senha para o Broker, bem como o seu endereço (host)
client.username_pw_set("brokerUser", password="brokerUserPassword")
client.connect(host="MEU.BROKER.MQTT.com", port=1883, keepalive=60)

# Inicia o loop
def mainMqttInterface():
    global client
    client.loop_forever()
    return 0

if __name__ == '__main__':
    print('Iniciando a interface EPM-MQTT...')
    sys.exit(int(not mainMqttInterface()) or 0)
