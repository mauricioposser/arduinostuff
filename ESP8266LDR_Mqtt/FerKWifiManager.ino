/* 
 *  Leitura da intensidade de luz via um sensor LDR enviando os dados para um Mqtt Broker
 *  Código para o ESP8266 NodeMcu
 *  Utiliza o WifiManager para configurar a rede e também o mqtt broker.
 *  Para resetar o sistema e entrar no modo de configuração, deve-se manter o botão pressionado por 2 segundos. 
 *  Portal de configuração no IP: 192.168.4.1 (padrão).
 *  
 *  Código e montagem inspirado nos seguintes (excelentes) canais do youtube:
 *  Fernando K Tecnologia: https://www.youtube.com/channel/UCIRfWLRDsdVOjJfpy6Ne04Q
 *  Infortrônica Para Zumbis: https://www.youtube.com/channel/UC05P95nXawYc15gIpN8GFPw
 *  
 *  Registro aqui meu muito obrigado a estes dois abnegados que compartilham seus conhecimentos com desconhecidos como eu! :)
 *  Agradeço também ao Tomaz Silveira pelas dicas e orientações ténicas para elaboração deste projeto.
 *  
 */

// Bibliotecas
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <DNSServer.h>
#include <WiFiManager.h>
#include <FS.h>
#include <ArduinoJson.h>
#include <PubSubClient.h>

#define DEBUG
#define LOOPDELAY 5000  // 5 segundos para cada leitura do sensor e publicação no Mqtt Broker
#define PULLDOWNRESISTOR 10000.0 // Resistor junto ao LDR

// Configuração padrão do Mqtt Broker
#define MQTTSERVER         "minhaconta.cloudmqtt.com"  //URL do servidor MQTT
#define MQTTSERVERPORT     "19620"                     //Porta do servidor MQTT
#define MQTTSERVERUSER     "meuusername"               //Usuário
#define MQTTSERVERPASSWORD "minhasenha"                //Senha
#define MQTTPUBTOPIC       "mspesp8266/pinldr"         //Tópico para publicar leitura do pino LDR

// Configuração dos pinos do ESP8266 NodeMcu
const int PIN_AP = D7;  //pino do botão para resetar a rede
const int PIN_LDR = A0; // pino LDR

float ldr = 0;      // LDR, entre 0 e 1023
float lux = 0.0;    // Valor da intensidade luminosa em LUX
bool saveWifiCfg = false;  // Controle para salvamento dos dados

WiFiClient espClient;            //Instância do WiFiClient
PubSubClient client(espClient);  //Passando a instância do WiFiClient para a instância do PubSubClient


/////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funções auxiliares
/////////////////////////////////////////////////////////////////////////////////////////////////////////

// *** Depuração ***
//Função para imprimir na porta serial
void printSerial(bool newLine, String msg)
{
  #ifdef DEBUG
    if(newLine)
      Serial.println(msg);
    else
      Serial.print(msg);
  #endif
}

// *** Leitura Sensor e conversão para Lux ***
// Função para leitura do sensor e composição da mensagem para publuicação no Mqtt Broker
String readSensor2Mqtt()
{

  /*
   * https://github.com/knolleary/pubsubclient
   * Limitations
    - It can only publish QoS 0 messages. It can subscribe at QoS 0 or QoS 1.
    - The maximum message size, including header, is 128 bytes by default. This is configurable via MQTT_MAX_PACKET_SIZE in PubSubClient.h.
    - The keepalive interval is set to 15 seconds by default. This is configurable via MQTT_KEEPALIVE in PubSubClient.h.
    - The client uses MQTT 3.1.1 by default. It can be changed to use MQTT 3.1 by changing value of MQTT_VERSION in PubSubClient.h.
   */
  
  /*  
  Esquema:
  Vi----|
        |
        |
       rLdr
        |---------- Vo
        |
  PULLDOWNRESISTOR
        |
        |
       GND

                PULLDOWNRESISTOR                                          Vi                Vi          Vi           1023.0
       Vo = ------------------------- * Vi   -> rLdr = PULLDOWNRESISTOR( ---- - 1 )  ; b = ---- = -------------- = ---------
             rLdr + PULLDOWNRESISTOR                                      Vo                Vo     ldr*Vi/1023.0      ldr
  
  */
  ldr = analogRead(PIN_LDR);
  float b = 1023.0 / ldr;
  float rLdr = PULLDOWNRESISTOR * (b - 1);
  float lux = 255.84 * pow(rLdr, -10/9) * 1000;  // http://home.roboticlab.eu/pt/examples/sensor/photoresistor
  return String(lux);
}
 

// *** WifiManager ***
//Função de retorno para notificar sobre a necessidade de salvar as configurações
void saveCallback()
{
  printSerial(true, "As configuracoes precisam ser salvas ...");
  saveWifiCfg = true;
}

// Callback que informa que o ESP entrou no modo AP
void configModeCallback (WiFiManager *myWiFiManager) {  
  Serial.println("Modo de configuração ...");
  Serial.println(WiFi.softAPIP()); //imprime o IP do AP
  Serial.println(myWiFiManager->getConfigPortalSSID()); //imprime o SSID criado da rede
}

// Callback que informa que as configs. da nova rede foram salvas (modo estação)
void saveConfigCallback () {
  Serial.println("Configuração salva!");
  Serial.println(WiFi.softAPIP()); //imprime o IP do AP
}

// *** MQTT (pub/sub) ***
//Função que reconecta ao servidor MQTT
void reconnectMqtt()
{
  printSerial(false, "Tentando conectar ao servidor MQTT...");
  //Tentativa de conectar. Se o MQTT precisa de autenticação, será chamada a função com autenticação, caso contrário, chama a sem autenticação. 
  bool conectado = strlen(MQTTSERVERUSER) > 0 ?
                   client.connect("ESP8266Client", MQTTSERVERUSER, MQTTSERVERPASSWORD) :
                   client.connect("ESP8266Client");
  if(conectado) {
    printSerial(true, "Mqtt Broker conectado!");
  } else {
    printSerial(false, "Falhou ao tentar conectar. Codigo: ");
    printSerial(false, String(client.state()).c_str());
    printSerial(true, " tentando novamente em 5 segundos");
    }
}

void disconnectMqtt()
{
  printSerial(true, "Fechando a conexao com o servidor MQTT...");
  client.disconnect();
}

//Função que envia os dados de intensidade luminosa.
void pubMqttBroker()
{
  if (!client.connected()) {
    printSerial(true, "MQTT desconectado! Tentando reconectar...");
    reconnectMqtt();
  }
  client.loop();
  //Publicando no MQTT
  printSerial(true, "Fazendo a publicacao...");
  String strMqttMsg = readSensor2Mqtt();
  char* msgMqtt = new char[strMqttMsg.length() + 1];
  strcpy(msgMqtt, strMqttMsg.c_str());
  // Publicação no tópico LDR
  client.publish(MQTTPUBTOPIC, msgMqtt, true);
  delete [] msgMqtt;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funções SETUP & LOOP
/////////////////////////////////////////////////////////////////////////////////////////////////////////

void setup()
{
  Serial.begin(9600);
  pinMode(PIN_AP, INPUT);    //Define o pino do botão
  pinMode(PIN_LDR, INPUT);   //Define o pino do LDR como entrada
  digitalWrite(PIN_AP, LOW); //Define o botão como desligado

  //Parâmetros extras para configuração via WifiManager
  //Depois de conectar, parameter.getValue() vai pegar o valor configurado.
  //Os campos do WiFiManagerParameter são: id do parâmetro, nome, valor padrão, comprimento
  WiFiManagerParameter custom_mqtt_server("server", "Servidor MQTT", MQTTSERVER, 40);
  WiFiManagerParameter custom_mqtt_port("port", "Porta", MQTTSERVERPORT, 6);
  WiFiManagerParameter custom_mqtt_user("user", "Usuario", MQTTSERVERUSER, 20);
  WiFiManagerParameter custom_mqtt_pass("pass", "Senha", MQTTSERVERPASSWORD, 20);
  WiFiManagerParameter custom_mqtt_topic_pub("topic_pub", "Topico para publicar", MQTTPUBTOPIC, 30);
  
  // Declaração do objeto wifiManager
  WiFiManager wifiManager;
  // Callback para quando entra em modo de configuração AP
  wifiManager.setAPCallback(configModeCallback); 
  //Definindo a função que informará a necessidade de salvar as configurações
  wifiManager.setSaveConfigCallback(saveCallback);
  //Adicionando os parâmetros para conectar ao servidor MQTT
  wifiManager.addParameter(&custom_mqtt_server);
  wifiManager.addParameter(&custom_mqtt_port);
  wifiManager.addParameter(&custom_mqtt_user);
  wifiManager.addParameter(&custom_mqtt_pass);
  wifiManager.addParameter(&custom_mqtt_topic_pub);
  wifiManager.autoConnect("ESP_AP","esp12349"); //cria uma rede com senha
  //Formatando a memória interna
  //(descomente a linha abaixo enquanto estiver testando e comente ou apague quando estiver pronto)
  SPIFFS.format();

  //////////////////////////////////////////////////////////////////////////////////////////////////////
  //Iniciando o SPIFSS (SPI Flash File System)
  printSerial(true, "Iniciando o SPIFSS (SPI Flash File System)");
  if (SPIFFS.begin()) {
    printSerial(true, "Sistema de arquivos SPIFSS montado!");
    if (SPIFFS.exists("/config.json")) {
      //Arquivo de configuração existe e será lido.
      printSerial(true, "Abrindo o arquivo de configuracao...");
      File configFile = SPIFFS.open("/config.json", "r");
      if (configFile) {
        printSerial(true, "Arquivo de configuracao aberto.");
        size_t size = configFile.size();
        //Alocando um buffer para armazenar o conteúdo do arquivo.
        std::unique_ptr<char[]> buf(new char[size]);
        configFile.readBytes(buf.get(), size);
        DynamicJsonBuffer jsonBuffer;
        JsonObject& json = jsonBuffer.parseObject(buf.get());
        json.printTo(Serial);
        if (json.success()) {
            //Copiando as variáveis salvas previamente no aquivo json para a memória do ESP.
            printSerial(true, "arquivo json analisado.");
            strcpy(MQTTSERVER, json["MQTTSERVER"]);
            strcpy(MQTTSERVERPORT, json["MQTTSERVERPORT"]);
            strcpy(MQTTSERVERUSER, json["MQTTSERVERUSER"]);
            strcpy(MQTTSERVERPASSWORD, json["MQTTSERVERPASSWORD"]);
            strcpy(MQTTPUBTOPIC, json["MQTTPUBTOPIC"]);
        } else {
          printSerial(true, "Falha ao ler as configuracoes do arquivo json.");
        }
      }
    }
  } else {
    printSerial(true, "Falha ao montar o sistema de arquivos SPIFSS.");
  }
  //Fim da leitura do sistema de arquivos SPIFSS
  //////////////////////////////////////////////////////////////////////////////////////////////////////

  //Busca o ID e senha da rede wifi e tenta conectar.
  //Caso não consiga conectar ou não exista ID e senha,
  //cria um access point com o nome "AutoConnectAP" e a senha "esp12349"
  //E entra em loop aguardando a configuração de uma rede WiFi válida.
  if (!wifiManager.autoConnect("AutoConnectAP", "esp12349")) {
    printSerial(true, "Falha ao conectar. Excedeu o tempo limite para conexao.");
    delay(3000);
    //Reinicia o ESP e tenta novamente ou entra em sono profundo (DeepSleep)
    ESP.reset();
    delay(5000);
  }
  //WiFi conectado
  printSerial(true, "Conectado!! :)");
  //Lendo os parâmetros atualizados do Mqtt Broker
  strcpy(MQTTSERVER, custom_mqtt_server.getValue());
  strcpy(MQTTSERVERPORT, custom_mqtt_port.getValue());
  strcpy(MQTTSERVERUSER, custom_mqtt_user.getValue());
  strcpy(MQTTSERVERPASSWORD, custom_mqtt_pass.getValue());
  strcpy(MQTTPUBTOPIC, custom_mqtt_topic_pub.getValue());
  //Salvando os parâmetros informados na tela web do WiFiManager
  if (saveWifiCfg)
  {
    printSerial(true, "Salvando as configuracoes");
    DynamicJsonBuffer jsonBuffer;
    JsonObject& json = jsonBuffer.createObject();
    json["MQTTSERVER"] = MQTTSERVER;
    json["MQTTSERVERPORT"] = MQTTSERVERPORT;
    json["MQTTSERVERUSER"] = MQTTSERVERUSER;
    json["MQTTSERVERPASSWORD"] = MQTTSERVERPASSWORD;
    json["MQTTPUBTOPIC"] = MQTTPUBTOPIC;
    File configFile = SPIFFS.open("/config.json", "w");
    if (!configFile)
    {
      printSerial(true, "Houve uma falha ao abrir o arquivo de configuracao para incluir/alterar as configuracoes.");
    }
    json.printTo(Serial);
    json.printTo(configFile);
    configFile.close();
  }
  printSerial(false, "IP: ");
  printSerial(true, WiFi.localIP().toString());
  //Informando ao client do PubSub a url do servidor e a porta.
  int portaInt = atoi(MQTTSERVERPORT);
  client.setServer(MQTTSERVER, portaInt);
}

void loop()
{
  WiFiManager wifiManager;
  //se o botão foi pressionado
   if ( digitalRead(PIN_AP) == HIGH ) {
      Serial.println("resetar"); //tenta abrir o portal
      if(!wifiManager.startConfigPortal("ESP_AP", "esp12349") ){
        Serial.println("Falha ao conectar");
        delay(2000);
        ESP.restart();
        delay(1000);
      }
      Serial.println("Conectou ESP_AP!!!");
   }
   else
   {
    pubMqttBroker();
    disconnectMqtt();
    delay(LOOPDELAY);
   }
}

