/*
 * 
 * VERIFICAR RELAY e WEBSERVER ESP AsyncTCP
 * https://randomnerdtutorials.com/esp8266-relay-module-ac-web-server/
 * 
 * https://github.com/me-no-dev/ESPAsyncWebServer
 * 
 * 
 * 
Equivalencia das saidas Digitais entre NodeMCU e ESP8266 (na IDE do Arduino)
NodeMCU - ESP8266
D0 = 16;
D1 = 5;
D2 = 4;
D3 = 0;
D4 = 2;
D5 = 14;
D6 = 12;
D7 = 13;
D8 = 15;
D9 = 3;
D10 = 1;
*/

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
//#include <ESP8266HTTPClient.h>
#include <FS.h>
#include <DNSServer.h>
#include <ArduinoJson.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <DHT.h>

// Porta Servidor Web
const byte        WEBSERVER_PORT          = 80;
// Headers do Servidor Web
const char*       WEBSERVER_HEADER_KEYS[] = {"User-Agent"};
// Porta Servidor DNS
const byte        DNSSERVER_PORT          = 53;
// Tamanho do Objeto JSON
const   size_t    JSON_SIZE               = JSON_OBJECT_SIZE(4) + 130; // 
// Instâncias da net
ESP8266WebServer  server(WEBSERVER_PORT);
DNSServer         dnsServer;
// Variáveis Globais Net
char              id[30];       // Identificação do dispositivo
char              ssid[30];     // Rede WiFi
char              pw[30];       // Senha da Rede WiFi
bool              relayon;       // Estado do relay
// Vars para SoftAp
const char *myssid = "ESP_PUMP";
const char *mypassword = "12345678";

IPAddress ip(10, 0, 0, 10);
IPAddress gateway(10, 0, 0, 1); //GATEWAY DE CONEXÃO (ALTERE PARA O GATEWAY DO SEU ROTEADOR)
IPAddress subnet(255,255,255,0); //MASCARA DE REDE

// Pinos da Cisterna
const int L_LEVEL_PIN  = 12; // D6
const int H_LELVEL_PIN = 4;  // D2

// Taking this into account, the safest ESP8266 pins to use with relays are: GPIO 5, GPIO 4, GPIO 14, GPIO 12 and GPIO 13.
// Additionally, some pins must be pulled HIGH or LOW in order to boot the ESP8266.
const int RELAY_PIN    = 5;  // D1 - estes so GIPIO Puros, iniciam em baixa: https://youtu.be/slKGGrPDNpk?t=706
const byte DHT22_PIN   = 2; // D4 - Deve operar pulled up por conta do Boot - DHT22 tbm, então blz! :)
const unsigned int LOOP_SCAN  = 5000; // Scan do Loop (ms)

int sensorLow  = 1; // BOMBA DESLIGADA
int sensorHigh = 1; // BOMBA DESLIGADA
int pumpON     = 0; // BOMBA DESLIGADA

DHT dht(DHT22_PIN, DHT22);

//#define DEBUG                   //Se descomentar esta linha vai habilitar a 'impressão' na porta serial

void printSerial(String msg, bool nline=true);
void hold(const unsigned int &ms);
void shutdownPump();
void turnOnPump();
String readDHTTemperature();
String readDHTHumidity();
// ******* Funções net *******
// Funções de Configuração ------------------------------
void  configReset();
boolean configRead();
boolean configSave();
// Requisições Web --------------------------------------
void handleIndex();
void handleInfos();
void handleConfig();
void handleConfigSave();
void handleReconfig();
void handleReboot();
//void handleCSS();
// Processa status do Relay -------------------------------
char relayStatus(char c);
// Processa requisoes HTTP ------------------------------
void handleRelayStatus();
void handleDHT();
// Extras
String deviceID();
String ipStr(const IPAddress &ip);
String hexStr(const unsigned long &h, const byte &l = 8);
// ******* Fim funções net *******

//Função inicial (será executado SOMENTE quando ligar o ESP)
void setup() {
  #ifdef DEBUG
    Serial.begin(9600);
  #endif
  printSerial("Iniciando setup...");
  pinMode(L_LEVEL_PIN, INPUT);
  pinMode(H_LELVEL_PIN, INPUT);
  pinMode(DHT22_PIN, INPUT);
  pinMode(RELAY_PIN, OUTPUT);
  //digitalWrite(RELAY_PIN, LOW);
  relayon = false;
  relaySet();

  // ******* Iniciando config. I/O net *******
  // SPIFFS
  if (!SPIFFS.begin()) {
    printSerial("SPIFFS ERRO");
    while (true);
  }
  // Lê configuração
  configRead();
  // Salva configuração
  configSave();
  // WiFi Access Point
  WiFi.hostname(deviceID());
  WiFi.softAP(myssid, mypassword);
  printSerial("WiFi AP - IP: " + ipStr(WiFi.softAPIP()));
  // Habilita roteamento DNS
  dnsServer.setErrorReplyCode(DNSReplyCode::ServerFailure);
  dnsServer.start(DNSSERVER_PORT, "*", WiFi.softAPIP());
  // WiFi Station
  WiFi.begin(ssid, pw);
  WiFi.config(ip, gateway, subnet);
  printSerial("Conectando WiFi " + String(ssid));
  byte b = 0;
  while(WiFi.status() != WL_CONNECTED && b < 60) {
    b++;
    printSerial(".");
    delay(500);
  }
  printSerial(" ");

  if (WiFi.status() == WL_CONNECTED) {
    // WiFi Station conectado
    printSerial("WiFi conectado (" + String(WiFi.RSSI()) + ") IP " + ipStr(WiFi.localIP()));
  } else {
    printSerial("WiFi não conectado");
  }
  // WebServer
  server.on(F("/index")    , handleIndex);
  server.on(F("/infos")    , handleInfos);
  server.on(F("/config")    , handleConfig);
  server.on(F("/configSave"), handleConfigSave);
  server.on(F("/reconfig")  , handleReconfig);
  server.on(F("/reboot")    , handleReboot);
  server.on("/relay", handleRelayStatus);
  server.onNotFound(handleIndex);
  server.collectHeaders(WEBSERVER_HEADER_KEYS, 1);
  server.begin();

  // ******* Fim das configs I/O net *******

  // Incio do OTA *************
  // Define funções de callback do processo
  // Início
  ArduinoOTA.onStart([](){
    String s;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      // Atualizar sketch
      s = "Sketch";
    } else { // U_SPIFFS
      // Atualizar SPIFFS
      s = "SPIFFS";
      // SPIFFS deve ser finalizada
      SPIFFS.end();
    }
    printSerial("Iniciando OTA - " + s);
  });

  // Fim
  ArduinoOTA.onEnd([](){
    printSerial("\nOTA Concluído.");
  });

  // Progresso
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    printSerial(String(progress * 100 / total));
    printSerial(" ");
  });

  // Falha
  ArduinoOTA.onError([](ota_error_t error) {
    printSerial("Erro " + String(error) + " ");
    if (error == OTA_AUTH_ERROR) {
      printSerial("Falha de autorização");
    } else if (error == OTA_BEGIN_ERROR) {
      printSerial("Falha de inicialização");
    } else if (error == OTA_CONNECT_ERROR) {
      printSerial("Falha de conexão");
    } else if (error == OTA_RECEIVE_ERROR) {
      printSerial("Falha de recebimento");
    } else if (error == OTA_END_ERROR) {
      printSerial("Falha de finalização");
    } else {
      printSerial("Falha desconhecida");
    }
  });

  // Inicializa OTA
  ArduinoOTA.begin();

  // Pronto
  printSerial("Atualização via OTA disponível.");
  //printSerial("Endereço IP: ");
  //printSerial(WiFi.localIP());
  // *** Fim do OTA do setup ***
  printSerial("Fim do setup!");
}


//Função de repetição (será executado INFINITAMENTE até o ESP ser desligado)
void loop()
{
   printSerial("Iniciando Loop ...");
   ArduinoOTA.handle();
   // ******* Net stuff *******
   // WatchDog ----------------------------------------
   yield();
   // DNS ---------------------------------------------
   dnsServer.processNextRequest();
   // Web ---------------------------------------------
   server.handleClient();
   // *************************

   sensorLow  = digitalRead(L_LEVEL_PIN);
   sensorHigh = digitalRead(H_LELVEL_PIN);
   pumpON     = digitalRead(RELAY_PIN);
   printSerial("LOW: ", false);
   printSerial(String(sensorLow));
   printSerial("HIGH: ", false);
   printSerial(String(sensorHigh));
   printSerial("RELAY: ", false);
   printSerial(String(pumpON));

   // Se sensor H está 0 (nivel alto) e bomba 1 -> desligar a bomba - Alta prioridade
   if(!sensorHigh && pumpON)
   {
     shutdownPump();
   }
   else
   {
    // Se sensor L está 1 (nivel baixo) e bomba 0 -> ligar a bomba
    if(sensorLow && !pumpON)
    {
      turnOnPump();
      hold(10000);// Liga a Bomba e aguarda 10s antes de seguir
    }
   }

   //printSerial(readDHTTemperature());
   //printSerial(readDHTHumidity());
   
   hold(LOOP_SCAN);
   printSerial("------- Fim do Loop -------");
}


// ***** FUNÇÕES AUXILIARES *****
void shutdownPump()
{
  printSerial("*** DESLIGAR BOMBA!!! ***");
  relayon = false;
  relaySet();
  printSerial("RELAY: ", false);
  printSerial(String(pumpON));
}

void turnOnPump()
{
  printSerial("*** LIGANDO BOMBA!!! ***");
  if(sensorHigh) // Sensor H está 1 (nivel baixo)
  { // Não atingiu nível alto, logo pode ligar a bomba!
    relayon = true;
    relaySet();
    printSerial("RELAY: ", false);
    printSerial(String(pumpON));
  }
  else
    printSerial("Bomba NÃO LIGADA - nível alto!");
}

//Função para imprimir na porta serial
void printSerial(String msg, bool nline)
{
  #ifdef DEBUG
    if(nline)
      Serial.println(msg);
    else
      Serial.print(msg);
  #endif
}

// Internet e Coisas - André Michelon
void hold(const unsigned int &ms)
{
  // Non blocking delay
  // Delay para evitar que ocorra reset por travamento do hardware
  // ATENÇÃO! O comando delay pode gerar TRAVAMENTO E RESET do ESP8266!!
  unsigned long m = millis();
  while (millis() - m < ms) {
    yield();
  }
}

// ******* Funções net *******
// Funções de Configuração ------------------------------
void  configReset() {
  // Define configuração padrão
  strlcpy(id, "MSP Pump-device", sizeof(id)); 
  strlcpy(ssid, "TELESCOPIUM", sizeof(ssid)); 
  strlcpy(pw, "telescopium123", sizeof(pw));
  relayon = false;
  relaySet();// Atualiza Relay
}

boolean configRead() {
  // Lê configuração
  StaticJsonDocument<JSON_SIZE> jsonConfig;
  File file = SPIFFS.open(F("/config.json"), "r");
  if (deserializeJson(jsonConfig, file)) {
    // Falha na leitura, assume valores padrão
    configReset();
    printSerial("Falha lendo CONFIG, assumindo valores padrão.");
    return false;
  } else {
    // Sucesso na leitura
    strlcpy(id, jsonConfig["id"]        | "", sizeof(id)); 
    strlcpy(ssid, jsonConfig["ssid"]    | "", sizeof(ssid)); 
    strlcpy(pw, jsonConfig["pw"]        | "", sizeof(pw));
    relayon     = jsonConfig["relayon"] | false;
    file.close();
    printSerial("\nLendo config:");
    serializeJsonPretty(jsonConfig, Serial);
    printSerial(" ");
    return true;
  }
}

boolean configSave() {
  // Grava configuração
  StaticJsonDocument<JSON_SIZE> jsonConfig;
  File file = SPIFFS.open(F("/config.json"), "w+");
  if (file) {
    // Atribui valores ao JSON e grava
    jsonConfig["id"]      = id;
    jsonConfig["ssid"]    = ssid;
    jsonConfig["pw"]      = pw;
    jsonConfig["relayon"] = false; //relayon; //\TODO futuramente eliminar este parâmetro não precisa ser salvo.
    serializeJsonPretty(jsonConfig, file);
    file.close();
    printSerial("\nGravando config:");
    serializeJsonPretty(jsonConfig, Serial);
    //printSerial(" ");
    return true;
  }
  return false;
}

// Requisições Web --------------------------------------
void handleIndex() {
  // INFOS
  File file = SPIFFS.open(F("/index.html"), "r");
  if (file) {
    file.setTimeout(100);
    String s = file.readString();
    file.close();
    // Atualiza conteúdo dinâmico
    s.replace(F("#highLevel#")       , String(sensorHigh));
    s.replace(F("#lowLevel#")      , String(sensorLow));
    s.replace(F("#relayState#")    , relayon ? F("Ligado") : F("Desligado"));
    s.replace(F("#temperature#"), readDHTTemperature());
    s.replace(F("#humidity#"), readDHTHumidity());    
    // Envia dados
    server.send(200, F("text/html"), s);
    printSerial("Home - Cliente: " + ipStr(server.client().remoteIP()) +
        (server.uri() != "/" ? " [" + server.uri() + "]" : ""));
  } else {
    server.send(500, F("text/plain"), F("Home - ERROR 500"));
    printSerial("Home - ERRO lendo arquivo");
  }
}

void handleInfos() {
  // INFOS
  File file = SPIFFS.open(F("/infos.html"), "r");
  if (file) {
    file.setTimeout(100);
    String s = file.readString();
    file.close();
    // Atualiza conteúdo dinâmico
    s.replace(F("#id#")       , id);
    s.replace(F("#relay#")      , relayon ? F("Ligado") : F("Desligado"));
    s.replace(F("#sysIP#")    , ipStr(WiFi.status() == WL_CONNECTED ? WiFi.localIP() : WiFi.softAPIP()));
    s.replace(F("#clientIP#") , ipStr(server.client().remoteIP()));
    s.replace(F("#userAgent#"), server.header(F("User-Agent")));
    s.replace(F("#temperature#"), readDHTTemperature());
    s.replace(F("#humidity#"), readDHTHumidity());    
    // Envia dados
    server.send(200, F("text/html"), s);
    printSerial("Home - Cliente: " + ipStr(server.client().remoteIP()) +
        (server.uri() != "/" ? " [" + server.uri() + "]" : ""));
  } else {
    server.send(500, F("text/plain"), F("Home - ERROR 500"));
    printSerial("Home - ERRO lendo arquivo");
  }
}

void handleConfig() {
  // Config
  File file = SPIFFS.open(F("/config.html"), "r");
  if (file) {
    file.setTimeout(100);
    String s = file.readString();
    file.close();
    // Atualiza conteúdo dinâmico
    s.replace(F("#id#")     , id);
    s.replace(F("#ssid#")   , ssid);
    // Send data
    server.send(200, F("text/html"), s);
    printSerial("Config - Cliente: " + ipStr(server.client().remoteIP()));
  } else {
    server.send(500, F("text/plain"), F("Config - ERROR 500"));
    printSerial("Config - ERRO lendo arquivo");
  }
}

void handleConfigSave() {
  // Grava Config
  if (server.args() >= 4) {
    String s;
    // Grava id
    s = server.arg("id");
    s.trim();
    if (s == "") {
      s = deviceID();
    }
    strlcpy(id, s.c_str(), sizeof(id));

    // Grava ssid
    s = server.arg("ssid");
    s.trim();
    strlcpy(ssid, s.c_str(), sizeof(ssid));

    // Grava pw
    s = server.arg("pw");
    s.trim();
    if (s != "") {
      // Atualiza senha, se informada
      strlcpy(pw, s.c_str(), sizeof(pw));
    }

    // Grava configuração
    if (configSave()) {
      server.send(200, F("text/html"), F("<html><meta charset='UTF-8'><script>alert('Configuração salva.');history.back()</script></html>"));
      printSerial("ConfigSave - Cliente: " + ipStr(server.client().remoteIP()));
    } else {
      server.send(200, F("text/html"), F("<html><meta charset='UTF-8'><script>alert('Falha salvando configuração.');history.back()</script></html>"));
      printSerial("ConfigSave - ERRO salvando Config");
    }
  } else {
    server.send(200, F("text/html"), F("<html><meta charset='UTF-8'><script>alert('Erro de parâmetros.');history.back()</script></html>"));
  }
}

void handleReconfig() {
  // Reinicia Config
  configReset();

  // Grava configuração
  if (configSave()) {
    server.send(200, F("text/html"), F("<html><meta charset='UTF-8'><script>alert('Configuração reiniciada.');window.location = '/'</script></html>"));
    printSerial("Reconfig - Cliente: " + ipStr(server.client().remoteIP()));
  } else {
    server.send(200, F("text/html"), F("<html><meta charset='UTF-8'><script>alert('Falha reiniciando configuração.');history.back()</script></html>"));
    printSerial("Reconfig - ERRO reiniciando Config");
  }
}

void handleReboot() {
  // Reboot
  File file = SPIFFS.open(F("/reboot.html"), "r");
  if (file) {
    server.streamFile(file, F("text/html"));
    file.close();
    printSerial("Reboot - Cliente: " + ipStr(server.client().remoteIP()));
    //delay(100);
    hold(100);
    ESP.restart();
  } else {
    server.send(500, F("text/plain"), F("Reboot - ERROR 500"));
    printSerial("Reboot - ERRO lendo arquivo");
  }
}


// Processa status do Relay  -------------------------------
char relayStatus(char c) {
  // Le/Define estado do Relay
  String s;
  char st;
  //boolean flPublish = true;
  if (c == '0') {
    // Relay desligado
    relayon = false;
    relaySet();
    s = "desligado";
    st = '0';
  } else if (c == '1') {
    // Relay ligado
    relayon = true;
    relaySet();
    s = "ligado";
    st = '1';
  }
  else {
    // Consulta
    s = "consulta";
    st = digitalRead(RELAY_PIN) ? '1' : '0';
  }
  // Exibe status
  printSerial(s);
  return st;
}

// Processa requisoes HTTP ------------------------------
void handleRelayStatus() {
  // Le/Define estado do Relay
  printSerial("[HTTP] ");
  char c = server.arg("set")[0];
  server.send(200, "text/plain", String(relayStatus(c)));
}

void handleDHT() {
  // Le DHT
  printSerial("[HTTP] ");
  char c = server.arg("set")[0];
  server.send(200, "text/plain", readDHTTemperature() + " - " + readDHTHumidity());
}

String deviceID() {
  // Retorna ID padrão
  #ifdef ESP8266
    // ESP8266 utiliza função getChipId()
    return "MSP-" + hexStr(ESP.getChipId());
  #else
    // ESP32 utiliza função getEfuseMac()
    return "IeC-" + hexStr(ESP.getEfuseMac());
  #endif
}

String ipStr(const IPAddress &ip) {
  // Retorna IPAddress em formato "n.n.n.n"
  String sFn = "";
  for (byte bFn = 0; bFn < 3; bFn++) {
    sFn += String((ip >> (8 * bFn)) & 0xFF) + ".";
  }
  sFn += String(((ip >> 8 * 3)) & 0xFF);
  return sFn;
}

void relaySet() {
  // Define estado do Relay
  digitalWrite(RELAY_PIN, relayon ? HIGH : LOW);
  // Adicionado hold para "garantir" que a leitura esteja ok
  hold(300);
  pumpON = digitalRead(RELAY_PIN);
}

String hexStr(const unsigned long &h, const byte &l) {
  // Retorna valor em formato hexadecimal
  String s;
  s= String(h, HEX);
  s.toUpperCase();
  s = ("00000000" + s).substring(s.length() + 8 - l);
  return s;
}

/*
 * DHT
 * 
 */

 String readDHTTemperature() {
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  //float t = dht.readTemperature(true);
  // Check if any reads failed and exit early (to try again).
  if (isnan(t)) {    
    printSerial("Failed to read from DHT sensor!");
    //return "--";
    return String(99);
  }
  else {
    printSerial(String(t));
    return String(t);
  }
}

String readDHTHumidity() {
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  if (isnan(h)) {
    printSerial("Failed to read from DHT sensor!");
    //return "--";
    return String(101);
  }
  else {
    printSerial(String(h));
    return String(h);
  }
}
