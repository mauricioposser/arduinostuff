/* Biblioteca Modbus utilizada:
 *  https://github.com/andresarmento/modbus-arduino
 *  André Sarmento Barbosa
 *  O código neste repositório é licenciado pela BSD New License.
 *  
 *  Alteração no padrão definido na biblioteca
 *  Modbus.h
 *  #define USE_HOLDING_REGISTERS_ONLY
 *      Dessa forma, somente as seguintes funções são suportadas:
 *      0x03 - Read Holding Registers
 *      0x06 - Write Single Register
 *      0x10 - Write Multiple Registers
 *  
 *      Adiciona registradores e configura valor inicial se especificado.
 *          void addHreg(word offset, word value)
 *      Configura um valor para o registrador.
 *          bool Hreg(word offset, word value)
 *      Retorna o valor de um registrador.
 *          word Hreg(word offset)
 */

// Modbus
#include <Modbus.h>
#include <ModbusSerial.h>

// DHT11
#include <DHT.h>

// Tempo de atualização (leituras/escritas)
#define SCANRATE 1000

// Define se é para apresentar resultados na porta serial
#define SHOWSERIAL false

// *** Modbus ***
// Modbus Registers Offsets (0-9999)
const int SENSOR_HREG_RELAY = 100;
const int SENSOR_HREG_HUMUDITY = 101;
const int SENSOR_HREG_TEMPERATURE = 102;
// ModbusSerial object
ModbusSerial mb;

// *** DHT11 ***
#define DHTPIN A0 // pino A0 - Arduino
#define DHTTYPE DHT11
// DHT object
DHT dht(DHTPIN, DHTTYPE);

const int signal2Relay = 4; // pino 4 - Arduino
// Global variables
long ts;
word relayON = 0; // unsigned int
word t = 0; // unsigned int
word h = 0; // unsigned int
float tf = 0.0;
float hf = 0.0;

void setup()
{
  pinMode(signal2Relay, OUTPUT); // Define o pino para o relay
  // *** DHT11 ***
  dht.begin(); // Inicializa o DHT11
  ts = millis();// Inicializando o contador de tempo

  if(SHOWSERIAL){
    Serial.begin(9600);
    Serial.println("Iniciando...");
  }
  else{
    // *** Modbus ***
    //SoftwareSerial myserial(3,3); // SoftwareSerial mySerial(10, 11); // RX, TX
    //mb.config(&myserial, 9600, SERIAL_8N1); // \TODO: verificar se precisa deste parâmetro: SERIAL_8N1
    mb.config(&Serial, 9600, SERIAL_8N1);
    
    mb.setSlaveId(1); // Definindo o ID do Slave (1-247)
    // Adicionando os Holding Registers
    mb.addHreg(SENSOR_HREG_RELAY, relayON); // Abre o relay
    mb.addHreg(SENSOR_HREG_HUMUDITY, (word)0); // Umidade em zero
    mb.addHreg(SENSOR_HREG_TEMPERATURE, (word)0); // Temperatura em zero 
  }
}

void loop()
{
  if(!SHOWSERIAL)
    mb.task(); // inicializa o objeto Modbus - deve ser chamado apenas uma vez dentro do LOOP
  //O código a seguir a cada SCANRATE
  if (millis() > ts + SCANRATE){
    ts = millis();
    // Lê o valor para o relay
    if(SHOWSERIAL){
      if (Serial.available()){
        char caractere = Serial.read();
        if ( isDigit(caractere) )          //relayON = (caractere - '0'); // Convertendo ASCII em valor numérico (0 = 48)
          relayON = (caractere - 0x30);
        else
          relayON = 0;
      }
    }
    else
      relayON = mb.Hreg(SENSOR_HREG_RELAY);
    //Liga/Desliga Relay
    if(relayON!=0)
      digitalWrite(signal2Relay, HIGH);
    else
      digitalWrite(signal2Relay, LOW);
    // A leitura da temperatura e umidade pode levar 250ms -o atraso do sensor pode chegar a 2 segundos.
    hf = dht.readHumidity();
    tf = dht.readTemperature();
    if(!isnan(tf) && !isnan(hf)){
      h = (word)hf;
      t = (word)tf;
    }
    else{
      h = (word) 999;
      t = (word) 999;
    }
    // Escrevendo os valores de umidade e temperatura
    if(SHOWSERIAL){
      Serial.print("Umidade(%): ");
      Serial.print(h);
      Serial.print("; ");
      Serial.print("Temp(*C): ");
      Serial.print(t);
      Serial.print("; ");
      Serial.print("Blower (on/off): ");
      Serial.println(relayON);
    }
    else{
      mb.Hreg(SENSOR_HREG_HUMUDITY, h);
      mb.Hreg(SENSOR_HREG_TEMPERATURE, t);
    }
  }
}

