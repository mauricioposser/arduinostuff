/********************************************************
 * Baseado  nos códigos do Canal
 * INTERNET E COISAS
 * Andre Michelon
 * https://internetecoisas.com.br
 */
/*
Equivalencia das saidas Digitais entre NodeMCU e ESP8266 (na IDE do Arduino)
NodeMCU - ESP8266
D0 = 16;
D1 = 5;
D2 = 4;
D3 = 0;
D4 = 2;
D5 = 14;
D6 = 12;
D7 = 13;
D8 = 15;
D9 = 3;
D10 = 1;
*/

// Bibliotecas ------------------------------------------
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <FS.h>
#include <DNSServer.h>
#include <TimeLib.h>
#include <WiFiUdp.h>
#include <ArduinoJson.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <Wire.h>  
#include "SSD1306Wire.h"
// Biblioteca PubSubClient por Ian Tester (Imroy) - Dica André Michelon!
// https://github.com/Imroy/pubsubclient
#include <PubSubClient.h>

//#define DEBUG  //Se descomentar esta linha vai habilitar a 'impressão' na porta serial

// Definição dos GPIO's - https://randomnerdtutorials.com/esp8266-pinout-reference-gpios/
// Pinos do OLED .96"  Conexão I2C - Driver SSD1306
const byte PINOLEDSDA = 4; // D2 - often used as SDA (I2C)
const byte PINOLEDSCL = 5; // D1 - often used as SCL (I2C)
// Porta do pino de sinal do DS18B20
const byte ONE_WIRE_BUS = 12; // D6

// Variáveis de processo - mantém temperaturas minima e maxima
float tempMin     = 0.0;
float tempMax     = 0.0;
float tempDS18    = 0.0;
float temp_SP     = 65.5;

int stage = 0;
/*
 * 0 - OFF          -> PUB_SCAN  = 2000
 * 1 - mashing      -> PUB_SCAN  = 2000
 * 2 - boiling      -> PUB_SCAN  = 2000
 * 3 - chilling     -> PUB_SCAN  = 2000
 * 4 - fermenting   -> PUB_SCAN  = 30000
 * 5 - maturing     -> PUB_SCAN  = 30000
 * 6 - priming      -> PUB_SCAN  = 30000
 */
const int LOOP_SCAN  = 2000; // Scan padrão do Loop (ms)
int PUB_SCAN   = 2000; // Scan padrão do Loop para escrita no Broker das leituras dos sensores (ms)
int PUB_COUNTER = 0; // Contador para auxiliar no controle do período de publicações
bool pub_broker = true; // Sempre que forem iguais, publicar!
int retentatives_broker = 0;

// Porta Servidor Web
const byte        WEBSERVER_PORT          = 80;
// Headers do Servidor Web
const char*       WEBSERVER_HEADER_KEYS[] = {"User-Agent"};
// Porta Servidor DNS
const byte        DNSSERVER_PORT          = 53;
// Tamanho do Objeto JSON
const   size_t    JSON_SIZE               = JSON_OBJECT_SIZE(4) + 130;// https://arduinojson.org/v6/assistant/

// Broker MQTT Cloud
const char* MQTT_SERVER      = "Endereço_do_broker";
const int   MQTT_PORT        = 1883;            // Porta
const char* MQTT_CLIENT      = "ID_Cliente";    // Cliente
const char* MQTT_USR         = "usuário";
const char* MQTT_PSS         = "senha";
const char* MQTT_TOPIC_SCAN  = "scan";          // Topico Scan (R)
const char* MQTT_TOPIC_DS18  = "temp2_ds18b20"; //Tópico para publicar Temp. DS18B20 (R)
const char* MQTT_TOPIC_T_SP = "temp_sp";        //Tópico para publicar Temp. SP (W)
const char* MQTT_TOPIC_STAGE = "tage";          // Topico Estágio (W)

// Instâncias -------------------------------------------
// Web Server
ESP8266WebServer  server(WEBSERVER_PORT);
WiFiClient  espClient;
// DNS Server
DNSServer         dnsServer;
// PubSub Client
PubSubClient client(espClient, MQTT_SERVER, MQTT_PORT);
// Define uma instancia do oneWire para comunicacao com o sensor
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
DeviceAddress sensor1;
// Inicializa o display Oled
SSD1306Wire  display(0x3C, PINOLEDSDA, PINOLEDSCL);
// Variáveis Globais ------------------------------------
char    id[30];       // Identificação do dispositivo
word    bootCount;    // Número de inicializações
char    ssid[30];     // Rede WiFi
char    pw[30];       // Senha da Rede WiFi
// Vars para SoftAp
const char *esp_ssid = "ESP_AP_CEVA";
const char *esp_password = "espapceva";
IPAddress ip(10,0,0,10); // Rede pessoal - IP fixo
IPAddress gateway(10,0,0,1); //GATEWAY DE CONEXÃO
IPAddress subnet(255,255,255,0); //MASCARA DE REDE

// Funções Genéricas ------------------------------------
void printSerial(String msg, bool nline=true);
void readSensorDS18B20();
void pubMqttBroker();
void hold(const unsigned int &ms);
String softwareStr();
String longTimeStr(const time_t &t);
String hexStr(const unsigned long &h, const byte &l = 8);
String deviceID();
String ipStr(const IPAddress &ip);
// Funções de Configuração ------------------------------
void  configReset();
boolean configRead();
boolean configSave();
// Requisições Web --------------------------------------
void handleHome();
void handleConfig();
void handleConfigSave();
void handleReconfig();
void handleReboot();
void handleCSS();
// Funções MQTT
void reconnectMqtt();
void disconnectMqtt();
void pubAuto();
void pubRelay();
void pubMqttBroker();
void callbackMqtt(const MQTT::Publish& pub);
// *** Funções para apresentar no OLED ***
void home();
void showMsg(String msg);
void progressBar();
void mostra_endereco_sensor(DeviceAddress deviceAddress);
bool HASNET = true;

// Setup -------------------------------------------
void setup() {
  #ifdef DEBUG
    Serial.begin(9600);
  #endif
  printSerial("Iniciando setup...");
  sensors.begin();
  if (!sensors.getAddress(sensor1, 0)) 
     printSerial("Sensores nao encontrados !"); 

if(HASNET)
{
  // SPIFFS
  if (!SPIFFS.begin()) {
    printSerial(F("SPIFFS ERRO"));
    configReset();
  }
  // Lê configuração
  configRead();
  // Incrementa contador de inicializações
  bootCount++;
  // Salva configuração
  configSave();
  // WiFi Access Point
  WiFi.hostname(deviceID());
  WiFi.mode(WIFI_AP_STA);
  WiFi.softAP(esp_ssid, esp_password);
  printSerial("WiFi AP " + deviceID() + " - IP " + ipStr(WiFi.softAPIP()));
  // Habilita roteamento DNS
  dnsServer.setErrorReplyCode(DNSReplyCode::ServerFailure);
  dnsServer.start(DNSSERVER_PORT, "*", WiFi.softAPIP());
  // WiFi Station
  WiFi.begin(ssid, pw);
  WiFi.config(ip, gateway, subnet);
  printSerial("Conectando WiFi " + String(ssid));
  byte b = 0;
  while(WiFi.status() != WL_CONNECTED && b < 60) {
    b++;
    printSerial(".", false);
    delay(500);
  }
  printSerial(" ");
  if (WiFi.status() == WL_CONNECTED) {
    // WiFi Station conectado
    printSerial("WiFi conectado (" + String(WiFi.RSSI()) + ") IP " + ipStr(WiFi.localIP()));
  } else {
    printSerial(F("WiFi não conectado"));
  }
  // WebServer
  server.on(F("/config")    , handleConfig);
  server.on(F("/configSave"), handleConfigSave);
  server.on(F("/reconfig")  , handleReconfig);
  server.on(F("/reboot")    , handleReboot);
  server.on(F("/css")       , handleCSS);
  server.onNotFound(handleHome);
  server.collectHeaders(WEBSERVER_HEADER_KEYS, 1);
  server.begin();
}  
  // Inicia o conteúdo do display
  display.init();
  display.flipScreenVertically();
  home();
  hold(800);
  progressBar();
  // Pronto
  printSerial(F("******************** Setup concluído!!! ********************"));
}

// Loop --------------------------------------------
void loop() {
  // WatchDog ----------------------------------------
  yield();
  printSerial("******************** Iniciando Loop ********************");
  if(HASNET)
  {
  // DNS ---------------------------------------------
  dnsServer.processNextRequest();
  // Web ---------------------------------------------
  server.handleClient();
  }

  // Lê infos dos sensores
  readSensorDS18B20();
  showMsg(String(tempDS18));
  // Mostra dados no serial monitor
  printSerial("Temp C: ", false);
  printSerial(String(tempDS18));
  printSerial(" Min : ", false);
  printSerial(String(tempMin));
  printSerial(" Max : ", false);
  printSerial(String(tempMax));

if(HASNET)
{
  pub_broker = ((PUB_SCAN - LOOP_SCAN) ? false : true);// ATENÇÃO: PUB_SCAN é sempre igual ou algum múltiplo de LOOP_SCAN!!!
  printSerial("****************> pub_broker *****");
  printSerial(String(pub_broker));
  printSerial(String(PUB_SCAN));
  printSerial(String(LOOP_SCAN));
  printSerial(String(PUB_SCAN - LOOP_SCAN));
  printSerial("*************************************");
  if(!pub_broker)
  {
    if( PUB_SCAN - PUB_COUNTER <= 0)
    {
      pub_broker = true;
      PUB_COUNTER=0;
    }
  }
  PUB_COUNTER += LOOP_SCAN;
  printSerial("***** PUB_COUNTER *****");
  printSerial(String(PUB_COUNTER));
  printSerial("**********************");
  // Processa conexao ao Broker
  // MQTT Client
  if(!client.connected())
  {
    reconnectMqtt();
    printSerial("------------  LOOP MQTT iniciado  ------------");
  }
  if(client.connected())
  {
    client.loop();
    if(pub_broker)
    {
      pubMqttBroker();
      printSerial("************* Leituras publicadas no Broker! *************");
    }
  }
}
    
  hold(LOOP_SCAN);
}


//////////////////////////////////////////////////
/////////////////////////////////////////////////

// Processa requisoes MQTT ------------------------------
void callbackMqtt(const MQTT::Publish& pub) {
  // Trata topicos MQTT recebidos
  printSerial("*** CALLBACK MQTT ***");
  
  if (pub.topic() == MQTT_TOPIC_STAGE) {
    printSerial("[MQTT_TOPIC_STAGE] ");
    char c = pub.payload_string()[0];
    stage = c - '0';
    if (stage > 3)
      PUB_SCAN = 30000; // 30s
    else
      PUB_SCAN = LOOP_SCAN;
    printSerial("stage: ", false);
    printSerial(String(stage));
    printSerial("PUB_SCAN: ", false);
    printSerial(String(PUB_SCAN));
    client.publish(MQTT::Publish(MQTT_TOPIC_SCAN, String(PUB_SCAN))
                    .set_retain()
                    .set_qos(1));
  }
  else if (pub.topic() == MQTT_TOPIC_T_SP) {
    printSerial("[MQTT_TOPIC_T_SP] ");
    String s = pub.payload_string();
    temp_SP = strtof(s.c_str(),0);
    printSerial("T_SP: ", false);
    printSerial(String(temp_SP));
  }
  printSerial("*** END CALLBACK ***");
}

void reconnectMqtt() {
  // Conecta ao Broker
  while (!client.connected()) {
    if (retentatives_broker > 3)
      break;
    Serial.println("Connectando Broker...");
    // Conecta
    if(client.connect(MQTT::Connect(MQTT_CLIENT)
                .set_clean_session()
                .set_will("status", "down")
                .set_auth(MQTT_USR, MQTT_PSS)
                .set_keepalive(30)
               ))
    {
      client.set_callback(callbackMqtt);
      client.subscribe(MQTT::Subscribe()
                  .add_topic(MQTT_TOPIC_T_SP, 1)
                  .add_topic(MQTT_TOPIC_STAGE, 1)
                 );
      retentatives_broker = 0;
      Serial.println("Conectado");
    } else {
      // Falha na conexao
      Serial.println("Falha");
      delay(5000);
      retentatives_broker += 1; // Se tentar 5 vezes e não der - não tenta mais até resetar!
    }
  }
}

//Função que envia os dados para o broker.
void pubMqttBroker()
{
  String msgMqttTds18 = (String(tempDS18));
  // Publicação nos tópicosdas leituras dos sensores
  client.publish(MQTT::Publish(MQTT_TOPIC_DS18, msgMqttTds18)
                    .set_retain()
                    .set_qos(1));
}

void  configReset() {
  // Define configuração padrão
  strlcpy(id, "Ceva_IoT_device", sizeof(id)); 
  strlcpy(ssid, "redelocal", sizeof(ssid)); 
  strlcpy(pw, "senha_rede_local", sizeof(pw));
  bootCount = 0;
}

boolean configRead() {
  // Lê configuração
  StaticJsonDocument<JSON_SIZE> jsonConfig;

  File file = SPIFFS.open(F("/Config.json"), "r");
  if (deserializeJson(jsonConfig, file)) {
    // Falha na leitura, assume valores padrão
    configReset();

    printSerial(F("Falha lendo CONFIG, assumindo valores padrão."));
    return false;
  } else {
    // Sucesso na leitura
    strlcpy(id, jsonConfig["id"]      | "", sizeof(id)); 
    strlcpy(ssid, jsonConfig["ssid"]  | "", sizeof(ssid)); 
    strlcpy(pw, jsonConfig["pw"]      | "", sizeof(pw)); 
    bootCount = jsonConfig["boot"]    | 0;
    file.close();
    printSerial(F("\nLendo config:"));
    serializeJsonPretty(jsonConfig, Serial);
    printSerial("");
    return true;
  }
}

boolean configSave() {
  // Grava configuração
  StaticJsonDocument<JSON_SIZE> jsonConfig;
  File file = SPIFFS.open(F("/Config.json"), "w+");
  if (file) {
    // Atribui valores ao JSON e grava
    jsonConfig["id"]    = id;
    jsonConfig["boot"]  = bootCount;
    jsonConfig["ssid"]  = ssid;
    jsonConfig["pw"]    = pw;
    serializeJsonPretty(jsonConfig, file);
    file.close();
    printSerial(F("\nGravando config:"));
    serializeJsonPretty(jsonConfig, Serial);
    printSerial("");
    return true;
  }
  return false;
}

// ***** Requisições Web *****
void handleHome() {
  // Home
  File file = SPIFFS.open(F("/home.html"), "r");
  if (file) {
    file.setTimeout(100);
    String s = file.readString();
    file.close();
    // Atualiza conteúdo dinâmico
    s.replace(F("#id#")       , id);
    s.replace(F("#bootCount#"), String(bootCount));
    s.replace(F("#serial#") , hexStr(ESP.getChipId()));
    s.replace(F("#software#") , softwareStr());
    s.replace(F("#sysIP#")    , ipStr(WiFi.status() == WL_CONNECTED ? WiFi.localIP() : WiFi.softAPIP()));
    s.replace(F("#clientIP#") , ipStr(server.client().remoteIP()));
    s.replace(F("#userAgent#"), server.header(F("User-Agent")));
    // Envia dados
    server.send(200, F("text/html"), s);
    printSerial("Home - Cliente: " + ipStr(server.client().remoteIP()) +
        (server.uri() != "/" ? " [" + server.uri() + "]" : ""));
  } else {
    server.send(500, F("text/plain"), F("Home - ERROR 500"));
    printSerial(F("Home - ERRO lendo arquivo"));
  }
}

void handleConfig() {
  // Config
  File file = SPIFFS.open(F("/config.html"), "r");
  if (file) {
    file.setTimeout(100);
    String s = file.readString();
    file.close();
    // Atualiza conteúdo dinâmico
    s.replace(F("#id#")     , id);
    s.replace(F("#ssid#")   , ssid);
    // Send data
    server.send(200, F("text/html"), s);
    printSerial("Config - Cliente: " + ipStr(server.client().remoteIP()));
  } else {
    server.send(500, F("text/plain"), F("Config - ERROR 500"));
    printSerial(F("Config - ERRO lendo arquivo"));
  }
}

void handleConfigSave() {
  // Grava Config
  // Verifica número de campos recebidos
  // ESP8266 gera o campo adicional "plain" via post
  if (server.args() == 5) {
    String s;
    // Grava id
    s = server.arg("id");
    s.trim();
    if (s == "") {
      s = deviceID();
    }
    strlcpy(id, s.c_str(), sizeof(id));
    // Grava ssid
    s = server.arg("ssid");
    s.trim();
    strlcpy(ssid, s.c_str(), sizeof(ssid));
    // Grava pw
    s = server.arg("pw");
    s.trim();
    if (s != "") {
      // Atualiza senha, se informada
      strlcpy(pw, s.c_str(), sizeof(pw));
    }
     // Grava configuração
    if (configSave()) {
      server.send(200, F("text/html"), F("<html><meta charset='UTF-8'><script>alert('Configuração salva.');history.back()</script></html>"));
      printSerial("ConfigSave - Cliente: " + ipStr(server.client().remoteIP()));
    } else {
      server.send(200, F("text/html"), F("<html><meta charset='UTF-8'><script>alert('Falha salvando configuração.');history.back()</script></html>"));
      printSerial(F("ConfigSave - ERRO salvando Config"));
    }
  } else {
    server.send(200, F("text/html"), F("<html><meta charset='UTF-8'><script>alert('Erro de parâmetros.');history.back()</script></html>"));
  }
}

void handleReconfig() {
  // Reinicia Config
  configReset();
  // Grava configuração
  if (configSave()) {
    server.send(200, F("text/html"), F("<html><meta charset='UTF-8'><script>alert('Configuração reiniciada.');window.location = '/'</script></html>"));
    printSerial("Reconfig - Cliente: " + ipStr(server.client().remoteIP()));
  } else {
    server.send(200, F("text/html"), F("<html><meta charset='UTF-8'><script>alert('Falha reiniciando configuração.');history.back()</script></html>"));
    printSerial(F("Reconfig - ERRO reiniciando Config"));
  }
}

void handleReboot() {
  // Reboot
  File file = SPIFFS.open(F("/reboot.html"), "r");
  if (file) {
    server.streamFile(file, F("text/html"));
    file.close();
    printSerial("Reboot - Cliente: " + ipStr(server.client().remoteIP()));
    delay(100);
    ESP.restart();
  } else {
    server.send(500, F("text/plain"), F("Reboot - ERROR 500"));
    printSerial(F("Reboot - ERRO lendo arquivo"));
  }
}

void handleCSS() {
  // Arquivo CSS
  File file = SPIFFS.open(F("/style.css"), "r");
  if (file) {
    // Define cache para 3 dias
    server.sendHeader(F("Cache-Control"), F("public, max-age=172800"));
    server.streamFile(file, F("text/css"));
    file.close();
    printSerial("CSS - Cliente: " + ipStr(server.client().remoteIP()));
  } else {
    server.send(500, F("text/plain"), F("CSS - ERROR 500"));
    printSerial(F("CSS - ERRO lendo arquivo"));
  }
}
// ***** END - Requisições Web *****

void readSensorDS18B20()
{
  sensors.requestTemperatures();
  float t = sensors.getTempC(sensor1);
  if (isnan(t))
    tempDS18 += 0.09;
  else
    tempDS18 = t;
  // Atualiza temperaturas minima e maxima
  if (tempDS18 < tempMin || tempMin == 0.0)
    tempMin = tempDS18;
  if (tempDS18 > tempMax)
    tempMax = tempDS18;
}

void mostra_endereco_sensor(DeviceAddress deviceAddress)
{
  for (uint8_t i = 0; i < 8; i++)
  {
    // Adiciona zeros se necessário
    if (deviceAddress[i] < 16) printSerial("0");
    printSerial(String(deviceAddress[i]));
  }
}
//// **** END - Sensores ****

// *** Funções para apresentar no OLED ***
void home()
{
  //Apaga o display
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  //Seleciona a fonte
  display.setFont(ArialMT_Plain_16);
  display.drawString(63, 0, "NodeMCU");
  display.drawString(63, 26, "ESP8266");
  display.drawString(63, 45, "Display Oled");
  display.display();
}

void showMsg(String msg)
{
  //Apaga o display
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  //Seleciona a fonte
  display.setFont(ArialMT_Plain_16);
  String minmax = String(tempMin, 1) + String("-") + String(tempMax, 1);
  display.drawString(63, 0, minmax.c_str());
  //display.drawString(63, 0, "Temp.(°C)");
  display.setFont(ArialMT_Plain_24);
  display.drawString(63, 30, msg.c_str());
  display.display();
}

void progressBar()
{
  for (int counter = 0; counter <= 100; counter++)
  {
    display.clear();
    display.setFont(ArialMT_Plain_16);
    //Desenha a barra de progresso
    display.drawProgressBar(0, 32, 120, 10, counter);
    //Atualiza a porcentagem completa
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.drawString(64, 15, String(counter) + "%");
    display.display();
    delay(10);
  }
}
// *** END - Funções para apresentar no OLED ***

// **** Funções Genéricas ****
//Função para imprimir na porta serial
void printSerial(String msg, bool nline)
{
  #ifdef DEBUG
    if(nline)
      Serial.println(msg);
    else
      Serial.print(msg);
  #endif
}

// Internet e Coisas - André Michelon
void hold(const unsigned int &ms)
{
  // Non blocking delay
  // Delay para evitar que ocorra reset por travamento do hardware
  // ATENÇÃO! O comando delay pode gerar TRAVAMENTO E RESET do ESP8266!!
  unsigned long m = millis();
  while (millis() - m < ms) {
    yield();
  }
}

String softwareStr() {
  // Retorna nome do software
  return String(__FILE__).substring(String(__FILE__).lastIndexOf("\\") + 1);
}

String hexStr(const unsigned long &h, const byte &l) {
  // Retorna valor em formato hexadecimal
  String s;
  s= String(h, HEX);
  s.toUpperCase();
  s = ("00000000" + s).substring(s.length() + 8 - l);
  return s;
}

String deviceID() {
  // Retorna ID padrão
  return "CevaIoT-" + hexStr(ESP.getChipId());
}

String ipStr(const IPAddress &ip) {
  // Retorna IPAddress em formato "n.n.n.n"
  String sFn = "";
  for (byte bFn = 0; bFn < 3; bFn++) {
    sFn += String((ip >> (8 * bFn)) & 0xFF) + ".";
  }
  sFn += String(((ip >> 8 * 3)) & 0xFF);
  return sFn;
}

// **** END - Funções Genéricas ****
